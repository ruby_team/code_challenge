class CreateFilers < ActiveRecord::Migration
  def change
    create_table :filers do |t|
      t.string :name
      t.string :attachment

      t.timestamps null: false
    end
  end
end
