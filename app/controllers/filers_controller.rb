class FilersController < ApplicationController
  before_action :set_filer, only: [:show, :destroy]

  def index
    @filers = Filer.all
  end

  def new
    @filer = Filer.new
  end
  
  def show
    @stackedWordTree = retrieve_stacked_word_trees()
  end

  def create
    @filer = Filer.new(filer_params)

    if @filer.save
      redirect_to filers_path, notice: "The file #{@filer.name} has been uploaded."
    else
      render "new"
    end
  end

  def destroy
    @filer.destroy
    redirect_to filers_path, notice:  "The file #{@filer.name} has been deleted."
  end

  private

  def filer_params
    params.require(:filer).permit(:name, :attachment)
  end

  def set_filer
    @filer = Filer.find(params[:id])
  end
end