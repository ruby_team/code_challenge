module FilersHelper
  # Algorithme builder the Stacked Word Trees
  def retrieve_stacked_word_trees
    @contents = reader()
    @contents.sort! {|item1,item2| item1.length<=>item2.length}
    array_word_sort_by_length = build_dictionary(@contents)

    stacked_word_trees = []
    stacked_word_trees << array_word_sort_by_length[0][0]    
    
    index_i = 0
    while (index_i+1) < array_word_sort_by_length.length
      index_j = 0
      while index_j < array_word_sort_by_length[index_i].length    
        checker = compar_letter_by_letter(array_word_sort_by_length[0][0], array_word_sort_by_length[index_i][index_j])
        
        if checker == true
          stacked_word_trees << array_word_sort_by_length[index_i][index_j]
        end
        
        index_j += 1
      end
      index_i += 1
    end
    
    return stacked_word_trees
  end

  private
  
  def reader
    file = File.open(@filer.attachment.file.file, "r") unless @filer.blank?
    str_array = []
    while !file.blank? && !file.eof? 
      str_array << file.readline
    end
    str_array
  end

  def build_dictionary(contents)
    array_word_sort_by_length = []
    count = 3
    while count<contents.length do
      index=contents.index{|item| item.length==count}
      next_index=contents.index{|item| item.length==(count+1)}

      if !index.nil? && !next_index.nil?
        array_word_sort_by_length[count] = contents[index..(next_index-1)]
      end
      count +=1
    end
    
    if array_word_sort_by_length[3].empty? 
      return nil 
    end

    array_word_sort_by_length[3..array_word_sort_by_length.count]
  end
  
  # Required: string1.length +1 == string2.length
  def compar_letter_by_letter(string1, string2)
    checker = []
    
    string1.each_byte { 
      checker << false  
    }
   
    index_j = 0
    while index_j < string2.length
      index_i = 0
      while index_i < string1.length
        if string1[index_i].eql?(string2[index_j]) == true
          checker.insert(index_i, true)
          break
        end

        index_i += 1            
      end

      index_j += 1
    end
    
    checker = checker.take(string1.length)
    checker.each { |checke| 
      if checke == false
        return false
      end
    }
    return true
  end
end